<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


if ($_SERVER['REQUEST_METHOD'] !== 'POST') :
    http_response_code(405);
    echo json_encode([
        'success' => 0,
        'message' => 'Invalid Request Method. HTTP method should be POST',
    ]);
    exit;
endif;

require 'database.php';
$database = new Database();
$conn = $database->dbConnection();

$data = json_decode(file_get_contents("php://input"));

if (!isset($data->id) || !isset($data->username) || !isset($data->name) || !isset($data->address) || !isset($data->contact)) :

    echo json_encode([
        'success' => 0,
        'message' => 'Please fill all the fields | id, username, name, address and contact',
    ]);
    exit;

elseif (empty(trim($data->id)) || empty(trim($data->username)) || empty(trim($data->name)) || empty(trim($data->address)) || empty(trim($data->contact))) :

    echo json_encode([
        'success' => 0,
        'message' => 'Oops! empty field detected. Please fill all the fields.',
    ]);
    exit;

endif;

try {
    $id = htmlspecialchars(trim($data->id));
    $username = htmlspecialchars(trim($data->username));
    $name = htmlspecialchars(trim($data->name));
    $address = htmlspecialchars(trim($data->address));
    $contact = htmlspecialchars(trim($data->contact));

    $query = "INSERT INTO `info`(id,username,name,address,contact) VALUES(:id,:username,:name,:address,:contact)";

    $stmt = $conn->prepare($query);

    $stmt->bindValue(':id', $id, PDO::PARAM_STR);
    $stmt->bindValue(':username', $username, PDO::PARAM_STR);
    $stmt->bindValue(':name', $name, PDO::PARAM_STR);
    $stmt->bindValue(':address', $address, PDO::PARAM_STR);
    $stmt->bindValue(':contact', $contact, PDO::PARAM_STR);

    if ($stmt->execute()) {

        http_response_code(201);
        echo json_encode([
            'success' => 1,
            'message' => 'Data Inserted Successfully.'
        ]);
        exit;
    }
    
    echo json_encode([
        'success' => 0,
        'message' => 'Data not Inserted.'
    ]);
    exit;

} catch (PDOException $e) {
    http_response_code(500);
    echo json_encode([
        'success' => 0,
        'message' => $e->getMessage()
    ]);
    exit;
}